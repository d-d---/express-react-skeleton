# 스켈레톤 프로젝트
──────────────────────────────────────────────────────────────────────────────
# First Uploading vscode code to github (vscode내에서 처음 깃헙에 업로드 시)
CHANGES + + 
=> commit massage + V 
=> git remote add origin https://github.com/kimja7045/React-Management-Tutorial.git <br/>
=> git push --set-upstream origin master

# Uploading vscode code to github (vscode내에서 깃헙에 업로드 시)
CHANGES + + 
=> commit massage + V 
=> MANAGEMENT … PUSH

# nodemon, body-parser, express Installation(한꺼번에 설치) + 노드몬 실행
npm install -g nodemon body-parser express => npm run server || nodemon server.js로 시작

# nodemon, body-parser, express Installation(설치)
혹시나 위 명령어로 설치 후 실행했을 때 안될 시
npm install --save-dev nodemon => npx nodemon
npm install --save express, npm install --save body-parser 등으로 설치

# Material UI(React의 부트스트랩 같은 프레임워크) Installation
npm install @material-ui/core
공식 문서 https://material-ui.com/ 를 활용하여 사용

# npm run dev
npm run dev(동시에 서버와 클라이언트를 실행) 로 실행 시 실행이 안되면
npm install concurrently -g 후 다시 npm run dev로 실행

# npm repo ()
ex) npm repo nodemon, npm repo express
노드몬에 대한 깃헙 리포지토리 브라우저가 생성

# MongoDB Installation and run
Window MSI 파일로 설치 - https://www.mongodb.com/download-center/community?jmp=homepage
기본 데이터베이스 디렉토리가 C:\data\db 이므로 실행 전에 폴더를 수동으로 만들어주셔야 합니다.

cmd - mongod 로 MongoDB 서버 실행
cmd - mongo - 클라이언트로 접속

use db_name - 사용할 데이터베이스 선택

show dbs    - 현재 데이터베이스 목록 확인
use db_name에서 db_name이 새로 생긴 db라면 아래 삽입 명령어 이후 show dbs를 해야
현재 db_name도 출력

db.sample.insert({"name":"sample"}); // 데이터 삽입 명령어

참조 - https://velopert.com/436, https://www.youtube.com/playlist?list=PL9FpF_z-xR_GMujql3S_XGV2SpdfDBkeC